var delugeApp = angular.module('delugeApp', []);

delugeApp.controller('LandscapeController', function LandscapeController($scope, $http) {
        var init = function () {
            $http.get('/backend/landscape/config', []).then(
                function (response) {
                    $scope.minSize = response.data.minSize;
                    $scope.maxSize = response.data.maxSize;
                    $scope.maxHeight = response.data.maxHeight;
                    $scope.landscapeSize = Math.floor(($scope.maxSize + $scope.minSize) / 2);
                }
            );
        };
        init();
        $scope.getGrid = function () {
            $scope.grid = [];
            $scope.landscape = [];
            $scope.water = [];
            $scope.filledVolume = null;
            for (var r = 0; r < $scope.maxHeight; r++) {
                var row = [];
                for (var c = 0; c < $scope.landscapeSize; c++) {
                    row.push(c);
                    if (r == 0) {
                        $scope.landscape.push({height: 0});
                        $scope.water.push(0);
                    }
                }
                $scope.grid.push(row);
            }
        };
        $scope.eraseWater = function () {
            $scope.filledVolume = null;
            $scope.water = [];
            for (var c = 0; c < $scope.landscapeSize; c++) {
                $scope.water.push(0);
            }
        };
        $scope.setHeight = function (colInd, height) {
            $scope.landscape[colInd].height = height;
            $scope.eraseWater();
        };
        $scope.checkIsGround = function (colInd, rowInd) {
            return $scope.landscape[colInd].height >= $scope.maxHeight - rowInd;
        };
        $scope.checkIsWater = function (colInd, rowInd) {
            var rowNum = $scope.maxHeight - rowInd;
            var h = $scope.landscape[colInd].height;
            return h < rowNum && rowNum <= h + $scope.water[colInd];
        };
        var random = function (min, max) {
            return Math.floor(Math.random() * (max - min + 1)) + min;
        };
        $scope.generateRandom = function () {
            $scope.eraseWater();
            var l = $scope.landscape;
            for (var i = 0; i < l.length; i++) {
                l[i].height = random(0, 10);
            }
        };
        $scope.getFilling = function () {
            var req = [];
            angular.forEach($scope.landscape, function (v, k) {
                req.push(v.height);
            });
            $http.post('/backend/water/fill', req).then(
                function (response) {
                    $scope.water = response.data.depths;
                    $scope.filledVolume = response.data.volume;
                }
            );
        }
    }
);