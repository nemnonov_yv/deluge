package ru.nemnonovyuri.testtasks.deluge.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import ru.nemnonovyuri.testtasks.deluge.dto.FillingDto;
import ru.nemnonovyuri.testtasks.deluge.exceptions.BadRequestException;
import ru.nemnonovyuri.testtasks.deluge.service.LandscapeService;
import ru.nemnonovyuri.testtasks.deluge.service.WaterService;

@Controller
@RequestMapping("/water")
public class WaterController {
    @Autowired
    private WaterService waterService;
    @Autowired
    private LandscapeService landscapeService;

    @RequestMapping(value = "/fill", method = RequestMethod.POST)
    @ResponseBody
    public FillingDto doFill(@RequestBody int[] heights) {
        if (!landscapeService.isValid(heights)) {
            throw new BadRequestException();
        }
        return waterService.getFilling(heights);
    }
}
