package ru.nemnonovyuri.testtasks.deluge.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import ru.nemnonovyuri.testtasks.deluge.dto.LandscapeConfigDto;
import ru.nemnonovyuri.testtasks.deluge.service.LandscapeService;

@Controller
@RequestMapping("/landscape")
public class LandscapeController {
    @Autowired
    private LandscapeService landscape;

    @RequestMapping(value = "config", method = RequestMethod.GET)
    @ResponseBody
    public LandscapeConfigDto getConfig() {
        return landscape.getLandscapeConfiguration();
    }
}
