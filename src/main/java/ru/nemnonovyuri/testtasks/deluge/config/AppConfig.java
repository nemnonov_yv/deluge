package ru.nemnonovyuri.testtasks.deluge.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
@EnableWebMvc
@ComponentScan("ru.nemnonovyuri.testtasks.deluge")
@PropertySource(name = "landscapeProps", value = "classpath:application.properties")
public class AppConfig extends WebMvcConfigurerAdapter {
    @Autowired
    private Environment env;

    @Bean
    public LandscapeConfiguration getLandscapeConfiguration() {
        return new LandscapeConfiguration(
                Integer.valueOf(env.getProperty("landscape_max_height")),
                Integer.valueOf(env.getProperty("landscape_min_size")),
                Integer.valueOf(env.getProperty("landscape_max_size"))
        );
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/static/**").addResourceLocations("/static/");
    }
}
