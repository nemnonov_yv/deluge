package ru.nemnonovyuri.testtasks.deluge.config;

public class LandscapeConfiguration {
    private Integer maxHeight;
    private Integer minSize;
    private Integer maxSize;

    public LandscapeConfiguration(Integer maxHeight, Integer minSize, Integer maxSize) {
        this.maxHeight = maxHeight;
        this.minSize = minSize;
        this.maxSize = maxSize;
    }

    public Integer getMaxHeight() {
        return maxHeight;
    }

    public Integer getMinSize() {
        return minSize;
    }

    public Integer getMaxSize() {
        return maxSize;
    }
}
