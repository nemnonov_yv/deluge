package ru.nemnonovyuri.testtasks.deluge.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.nemnonovyuri.testtasks.deluge.config.LandscapeConfiguration;
import ru.nemnonovyuri.testtasks.deluge.dto.LandscapeConfigDto;

@Service
public class LandscapeService {
    @Autowired
    private LandscapeConfiguration landscapeConfiguration;

    public boolean isValid(int[] heights) {
        if (heights.length > landscapeConfiguration.getMaxSize()) {
            return false;
        }
        for (int h : heights) {
            if (h < 0 || h > landscapeConfiguration.getMaxHeight()) {
                return false;
            }
        }
        return true;
    }

    public LandscapeConfigDto getLandscapeConfiguration() {
        return new LandscapeConfigDto(landscapeConfiguration);
    }
}
