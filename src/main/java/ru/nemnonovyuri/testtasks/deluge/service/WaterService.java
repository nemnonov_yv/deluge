package ru.nemnonovyuri.testtasks.deluge.service;

import org.springframework.stereotype.Service;
import ru.nemnonovyuri.testtasks.deluge.dto.FillingDto;

import java.util.ArrayList;
import java.util.List;

import static java.lang.Math.max;
import static java.lang.Math.min;

@Service
public class WaterService {
    public FillingDto getFilling(int[] landscape) {
        int[] depthArray = getFilledDepths(landscape);
        FillingDto filling = new FillingDto();
        filling.setDepths(depthArray);
        filling.setVolume(getFilledVolume(depthArray));
        return filling;
    }

    /**
     * Calculates volume for given array of depths
     *
     * @param filled array with depths. Refer to {@link WaterService#getFilledDepths(int[])}
     * @return total filling volume
     */
    public int getFilledVolume(int[] filled) {
        int volume = 0;
        for (int depth : filled) {
            volume += depth;
        }
        return volume;
    }

    /**
     * Calculates filling volume for given array of landscape heights
     *
     * @param landscape each item this array represents ground height at given position
     * @return total filling volume
     */
    public int fillAndGetVolume(int[] landscape) {
        return getFilledVolume(getFilledDepths(landscape));
    }

    /**
     * Calculates filling depth for each position
     *
     * @param landscape each item this array represents ground height at given position
     * @return array with depths after filling
     */
    public int[] getFilledDepths(int[] landscape) {
        int[] depths = new int[landscape.length];
        if (landscape.length <= 2) {
            return depths;
        }
        List<Peak> peaks = getPeaks(landscape);
        int peakCount = peaks.size();
        if (peakCount <= 1 || peakCount == landscape.length) {
            return depths;
        }
        int highestPeakIdx = 0;
        Peak left = peaks.get(highestPeakIdx);
        for (int i = 1; i < peakCount; i++) {
            Peak current = peaks.get(i);
            if (current.height >= left.height) {
                if (current.pos - left.pos > 1) {
                    fill(landscape, left.pos, current.pos, depths);
                }
                left = current;
                highestPeakIdx = i;
            }
        }
        Peak right = peaks.get(peakCount - 1);
        for (int i = peakCount - 2; i >= highestPeakIdx; i--) {
            Peak current = peaks.get(i);
            if (current.height >= right.height) {
                if (right.pos - current.pos > 1) {
                    fill(landscape, current.pos, right.pos, depths);
                }
                right = current;
            }
        }
        return depths;
    }

    private void fill(int[] landscape, int from, int to, int[] depths) {
        int level = min(landscape[from], landscape[to]);
        for (int i = from; i < to; i++) {
            depths[i] = max(level - landscape[i], 0);
        }
    }

    /**
     * Finds landscape peaks. Raised plane considered as sequence of peaks
     * @param land array with heights
     * @return List of {@link Peak} instances
     */
    private List<Peak> getPeaks(int[] land) {
        boolean rising = false;
        boolean lowering = false;
        Integer plainStart = null;
        List<Peak> peaks = new ArrayList<>();
        int width = land.length;
        for (int i = 1; i < width; i++) {
            if (land[i - 1] == land[i]) {
                plainStart = plainStart == null ? i - 1 : plainStart;
            }
            if (land[i - 1] > land[i] && !lowering) {
                lowering = true;
                rising = false;
                if (plainStart != null) {
                    for (int j = plainStart; j < i; j++) {
                        peaks.add(new Peak(j, land[j]));
                    }
                    plainStart = null;
                } else {
                    peaks.add(new Peak(i - 1, land[i - 1]));
                }
            }
            if (land[i - 1] < land[i]) {
                lowering = false;
                rising = true;
                plainStart = null;
            }
            if (i + 1 == width && rising) {
                peaks.add(new Peak(i, land[i]));
            }
        }
        return peaks;
    }

    private static class Peak {
        int pos;
        int height;

        public Peak(int pos, int height) {
            this.pos = pos;
            this.height = height;
        }
    }
}
