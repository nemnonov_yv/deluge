package ru.nemnonovyuri.testtasks.deluge.dto;

import ru.nemnonovyuri.testtasks.deluge.config.LandscapeConfiguration;

public class LandscapeConfigDto {
    private int maxHeight;
    private int minSize;
    private int maxSize;

    public LandscapeConfigDto(int minSize, int maxSize, int maxHeight) {
        this.minSize = minSize;
        this.maxSize = maxSize;
        this.maxHeight = maxHeight;
    }

    public LandscapeConfigDto(LandscapeConfiguration config) {
        this(config.getMinSize(), config.getMaxSize(), config.getMaxHeight());
    }

    public int getMaxHeight() {
        return maxHeight;
    }

    public void setMaxHeight(int maxHeight) {
        this.maxHeight = maxHeight;
    }

    public int getMinSize() {
        return minSize;
    }

    public void setMinSize(int minSize) {
        this.minSize = minSize;
    }

    public int getMaxSize() {
        return maxSize;
    }

    public void setMaxSize(int maxSize) {
        this.maxSize = maxSize;
    }
}
