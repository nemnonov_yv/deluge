package ru.nemnonovyuri.testtasks.deluge.dto;

public class FillingDto {
    private int volume;
    private int[] depths;

    public FillingDto() {
    }

    public int getVolume() {
        return volume;
    }

    public void setVolume(int volume) {
        this.volume = volume;
    }

    public int[] getDepths() {
        return depths;
    }

    public void setDepths(int[] depths) {
        this.depths = depths;
    }
}
