package ru.nemnonovyuri.testtasks.deluge.service;

import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class WaterServiceTest  {
    private static WaterService service;

    @BeforeClass
    public static void createService() {
        service = new WaterService();
    }

    @Test
    public void testTaskExample1() throws Exception {
        int[] landscape = new int[]{3, 2, 4, 1, 2};
        assertEquals(service.fillAndGetVolume(landscape), 2);
    }

    @Test
    public void testTaskExample2() throws Exception {
        int[] landscape = new int[]{4, 1, 1, 0, 2, 3};
        assertEquals(service.fillAndGetVolume(landscape), 8);
    }

    @Test
    public void testZeroVolumeWithOnePeak() throws Exception {
        int[] landscape = new int[]{1, 2, 3, 4, 5, 4, 3, 2, 1};
        assertEquals(service.fillAndGetVolume(landscape), 0);
    }

    @Test
    public void testZeroVolumeOnPlain() throws Exception {
        int[] landscape = new int[]{1, 1, 1, 1, 1, 1, 1, 1};
        assertEquals(service.fillAndGetVolume(landscape), 0);
    }

    @Test
    public void testZeroVolumeOnTwoItems() throws Exception {
        int[] landscape = new int[]{1, 9};
        assertEquals(service.fillAndGetVolume(landscape), 0);
    }

    @Test
    public void testDescendingPeaks() throws Exception {
        int[] landscape = new int[]{9, 0, 8, 0, 7, 0, 6, 0};        // 8 + 7 + 6 = 21
        assertEquals(service.fillAndGetVolume(landscape), 21);
    }

    @Test
    public void testAscendingPeaks() throws Exception {
        int[] landscape = new int[]{3, 0, 4, 0, 5, 0, 6, 0, 7};        // 3 + 4 + 5 + 6 = 18
        assertEquals(service.fillAndGetVolume(landscape), 18);
    }

    @Test
    public void testHighLowHighPeaks() throws Exception {
        int[] landscape = new int[]{3, 4, 5, 2, 4, 1, 6, 2};        // (5 - 2) + (5 - 4) + (5 - 1) = 8
        assertEquals(service.fillAndGetVolume(landscape), 8);
    }

    @Test
    public void testLowHighLowPeaks() throws Exception {
        int[] landscape = new int[]{3, 5, 2, 8, 1, 6, 2};        // (5 - 2) + (6 - 1) = 8
        assertEquals(service.fillAndGetVolume(landscape), 8);
    }

    @Test
    public void testRandomPeaks() throws Exception {
        int[] landscape = new int[]{4, 8, 7, 3, 5, 4, 3, 7, 1, 2, 3, 5, 4, 6};  // (7-3)+(7-5)+(7-4)+(7-3)+(6-1)+(6-2)+(6-3)+(6-5)+(6-4)=28
        assertEquals(service.fillAndGetVolume(landscape), 28);
    }

    @Test
    public void testSameHeightBorders() throws Exception {
        int[] landscape = new int[]{5, 1, 1, 1, 5};  // (5-1)*3=12
        assertEquals(service.fillAndGetVolume(landscape), 12);
    }
}